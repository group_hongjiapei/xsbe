FROM hongjiapei/nginx-php

COPY nginx.conf /usr/local/nginx/conf.d/nginx.conf

COPY src /data/src

RUN chown -R www-data.www-data /data/src/ && chmod -R 755 /data/src \
    && rm -rf /usr/local/php/etc/php-fpm.d/www.conf

WORKDIR /data/src

EXPOSE 80

CMD sh -c 'nginx && php-fpm -D && tail -f /usr/local/nginx/logs/error.log'
